extends Node

# Script containing most constant for the Artiphonie app

# ===== GOOSE GAME =====
const GOOSE_GAME_NAME := "Jeu de l'oie"
const PATH_GOOSE_GAME := "res://artiphonie/goose_game/goose_game.tscn"
const PATH_GOOSE_GAME_ICON := "res://assets/icons/goosePlate.png"
const GOOSE_GAME_DIFFICULTY := ["Facile", "Normal", "Difficile"]
# ===== ===== =====

# ===== LISTEN AND CHOOSE =====
const LISTEN_AND_CHOOSE_GAME_NAME := "Ecoute et Choisis"
const PATH_LISTEN_AND_CHOOSE_GAME := "res://artiphonie/listen_choose/listen_choose.tscn"
const PATH_LISTEN_AND_CHOOSE_GAME_ICON := "res://assets/icons/ecoute.png"
const LISTEN_AND_CHOOSE_GAME_DIFFICULTY := ["[..] Phonetique", "Facile", "Normal", "Difficile"]
# ===== ===== =====

# ===== MEMORY =====
const MEMORY_NAME := "Jeu du mémorie"
const PATH_MEMORY := "res://artiphonie/memory/memory_game.tscn"
const PATH_MEMORY_ICON := "res://assets/icons/card.png"
const MEMORY_DIFFICULTY := ["Facile", "Normal", "Difficile"]
# ===== ===== =====

const PATH_LEARNING := "res://artiphonie/learning/learning.tscn"
const PATH_TRAINING := "res://artiphonie/training/training.tscn"
const PATH_PLAYING := "res://artiphonie/playing/playing.tscn"

const PATH_PRONOUNCING := "res://artiphonie/utility/pronouncing/pronouncing.tscn"

const PATH_PHONETIC_TABLE_SORTED := "res://data/phonetic_table_sorted.json"

const WORD_ICON_PATH := "res://art/images/"
